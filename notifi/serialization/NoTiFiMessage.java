/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import tools.BinaryTools;
import tools.Filters;

/**
 * NoTiFi message
 */
public abstract class NoTiFiMessage {
	public static final int VERSION = 3, // stamp with current version
							HEADER_SIZE = 2;
	protected int msgId;
	
	/**
	 * Deserializes from byte array
	 * 
	 * @param pkt byte array to deserialize
	 * 
	 * @return a specific NoTiFi message resulting from deserialization
	 * 
	 * @throws IllegalArgumentException
	 * 		if bad version or code
	 * @throws IOException
	 * 		if I/O problem including packet too long/short (EOFException)
	 */
	public static NoTiFiMessage decode(byte[] pkt) throws IllegalArgumentException, IOException {
		// Test minimum sized packet
		if(pkt.length < HEADER_SIZE) {
			throw new IOException("Short packet");
		}
		
		// init wrapper
		ByteBuffer bb = ByteBuffer.wrap(pkt)
				.order(ByteOrder.LITTLE_ENDIAN); // order as specified
		
		// first byte of packet
		// must be 8 bits
		String b = BinaryTools.formatBits(bb.get(), 8);
		
		// version test
		int v = Integer.parseInt(b.substring(0, 4), 2);
		if(v != VERSION) {
			throw new IllegalArgumentException("Bad Version");
		}
		
		// test possible code
		int code = Integer.parseInt(b.substring(4, 8), 2);
		if(code < 0 || code > 5) {
			throw new IllegalArgumentException("Bad Code");
		}
		
		return switchSub(bb, code);
	}
	
	/**
	 * Switches deserialization routine based off code
	 * 
	 * @param bb ByteBuffer to deserialize
	 * @param code specific NoTiFi code
	 * 
	 * @return a specific NoTiFi message resulting from deserialization
	 * 
	 * @throws IOException
	 * 		if I/O problem including packet too long/short (EOFException)
	 */
	private static NoTiFiMessage switchSub(ByteBuffer bb, int code) throws IOException {
		// init msgID
		int msgId = Byte.toUnsignedInt( bb.get() );
		// decoded NoTiFiMessage
		NoTiFiMessage ntf = null;
		
		try {
			switch(code) {
				case NoTiFiRegister.CODE: ntf = new NoTiFiRegister(msgId, bb);
					break;
				case NoTiFiLocationAddition.CODE: ntf = new NoTiFiLocationAddition(msgId, NoTiFiLocation.decode(bb));
					break;
				case NoTiFiLocationDeletion.CODE: ntf = new NoTiFiLocationDeletion(msgId, NoTiFiLocation.decode(bb));
					break;
				case NoTiFiDeregister.CODE: ntf = new NoTiFiDeregister(msgId, bb);
					break;
				case NoTiFiError.CODE:  ntf = new NoTiFiError(msgId, bb);
					break;
				case NoTiFiACK.CODE: ntf = new NoTiFiACK(msgId);
				// no default because code bound was enforced already
			}
		} catch(BufferUnderflowException e) {
			throw new IOException("Short packet", e);
		}

		// Test for extra bytes AND test for extra zeroes
		if(bb.hasRemaining() && bb.get() != 0) {
			throw new IOException("Large Packet");
		}
		
		return ntf;
	}
	
	/**
	 * Serializes message
	 * 
	 * @return serialized message bytes
	 */
	public abstract byte[] encode();
	
	/**
	 * Encodes NoTiFiMessage header
	 * 
	 * @param pcktSize size of packet to allocate in bytes
	 * @param code specific msg code
	 * 
	 * @return ByteBuffer with header for fluid interfaces
	 */
	protected ByteBuffer encodeHeader(int pcktSize, int code) {
		// convert ints to bits and parse into bytes
		return ByteBuffer.allocate(pcktSize)
				.put( Byte.parseByte(
						BinaryTools.formatBits(VERSION, 4) + BinaryTools.formatBits(code, 4)
						, 2) ) // add v+code
				.put((byte)msgId); // add msgID
	}
	
	/**
	 * Returns total bytes of this NoTiFiMessage
	 * 
	 * @return bytes
	 */
	public abstract int getBytes();
	
	/**
	 * Set message ID
	 * 
	 * @param msgId message ID
	 * 
	 * @throws IllegalArgumentException
	 * 		if message ID is out of range
	 */
	public void setMsgId(int msgId) throws IllegalArgumentException {
		this.msgId = (int)Filters.filterId(msgId, 8);
	}
	
	/**
	 * Get message ID
	 * 
	 * @return message ID
	 */
	public int getMsgId() {
		return msgId;
	}

	/**
	 * Get operation code
	 * 
	 * @return operation code
	 */
	public abstract int getCode();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + msgId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoTiFiMessage other = (NoTiFiMessage) obj;
		if (msgId != other.msgId)
			return false;
		return true;
	}
}
