/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

import java.io.IOException;
import java.net.Inet4Address;
import java.nio.ByteBuffer;

/**
 * NoTiFi register
 */
public class NoTiFiRegister extends NoTiFiRegisterOption {
	// Code for NoTiFi register
	public static final int CODE = 0;
	
	public NoTiFiRegister(int msgId, ByteBuffer bb) throws IllegalArgumentException, IOException {
		super(msgId, bb);
	}

	public NoTiFiRegister(int msgId, Inet4Address address, int port) throws IllegalArgumentException {
		super(msgId, address, port);
	}
	
	@Override
	public byte[] encode() {
		return encodeReg(CODE);
	}
	
	@Override
	public int getCode() {
		return CODE;
	}
}
