/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import latitude.serialization.MessageInput;
import tools.BinaryTools;
import tools.Filters;

/**
 * NoTiFi location
 */
public class NoTiFiLocation {
	public static final String CHARENC = MessageInput.CHARENC;
	// NoTiFiLocation required bytes minus String lengths
	public static final int BYTES = 22,
							USERID_BYTES = Integer.BYTES,
							MAX_STRING = 256;
	private long userId;
	private double longitude, latitude;
	private String locationName, locationDescription;
	
	/**
	 * Constructs location with set values
	 * 
	 * @param userId ID for user
	 * @param longitude position of location
	 * @param latitude position of location
	 * @param locationName name of location
	 * @param locationDescription description of location
	 * 
	 * @throws IllegalArgumentException
	 * 		if parameter validation fails
	 */
	public NoTiFiLocation(long userId, double longitude, double latitude, String locationName, String locationDescription)
			throws IllegalArgumentException {
		this.setUserId(userId);
		this.setLongitude(longitude);
		this.setLatitude(latitude);
		this.setLocationName(locationName);
		this.setLocationDescription(locationDescription);
	}
	
	/**
	 * Serializes NoTiFiLocation
	 * 
	 * @return serialized Location bytes
	 */
	public byte[] encode() {
		try {
			// memory allocation
			return ByteBuffer.allocate( getBytes() )
					.putInt((int)userId) // specified 4 bytes, so cast to int
					.order(ByteOrder.LITTLE_ENDIAN) // userID has different order
					.putDouble(longitude) // add longitude
					.putDouble(latitude) // add latitude
					.put( (byte)locationName.length() ) // encode length
					.put( locationName.getBytes(CHARENC) ) // add locationName
					.put( (byte)locationDescription.length() ) // encode length
					.put( locationDescription.getBytes(CHARENC) ) // add locationDescription
					.array();
		} catch (UnsupportedEncodingException e) {
			// ASCII will be supported
		}
		return null; // compiler
	}
	
	/**
	 * Deserializes NoTiFiLocation
	 * 
	 * @param bb ByteBuffer to deserialize
	 * 
	 * @return deserialized NoTiFiLocation
	 */
	public static NoTiFiLocation decode(ByteBuffer bb) {
		return new NoTiFiLocation(
				 // unsigned, also switch ordering
				(long)bb.order(ByteOrder.BIG_ENDIAN).getInt()&0xffffffffL, // promote to long, then AND to keep int bits
				// switch ordering back
				bb.order(ByteOrder.LITTLE_ENDIAN).getDouble(),
				bb.getDouble(), // read long/lat
				decodeLocationString(bb),
				decodeLocationString(bb));
	}
	
	/**
	 * Factory method decoding of String from ByteBuffer of NoTiFi packet
	 * 
	 * @param bb ByteBuffer to deserialize
	 * 
	 * @return decoded String from packet
	 */
	private static String decodeLocationString(ByteBuffer bb) {
		try {
			// will read the amount of bytes specified by the next immediate byte
			return new String(BinaryTools.getBuffBytes(bb, Byte.toUnsignedInt( bb.get() )), CHARENC);
		} catch (UnsupportedEncodingException e) {
			// ASCII will be supported
		}
		return null; // compiler
	}
	
	/**
	 * Returns total bytes of this NoTiFiLocation
	 * 
	 * @return bytes
	 */
	public int getBytes() {
		return BYTES + locationName.length() + locationDescription.length();
	}

	/**
	 * Returns user ID
	 * 
	 * @return user ID
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * Sets user ID
	 * 
	 * @param userId new user ID
	 * 
	 * @throws IllegalArgumentException
	 * 		if user ID out of range
	 */
	public void setUserId(long userId) throws IllegalArgumentException {
		this.userId = Filters.filterId(userId, Filters.UNSIGNED_INT_MAX_POWER);
	}

	/**
	 * Returns longitude
	 * 
	 * @return longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Sets longitude
	 * 
	 * @param longitude new longitude
	 * 
	 * @throws IllegalArgumentException
	 * 		if longitude out of range
	 */
	public void setLongitude(double longitude) throws IllegalArgumentException {
		this.longitude = Filters.filterLongLat(String.valueOf(longitude), Filters.LONGITUDE_MAX);
	}

	/**
	 * Returns latitude
	 * 
	 * @return latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Sets latitude
	 * 
	 * @param latitude new latitude
	 * 
	 * @throws IllegalArgumentException
	 * 		if latitude out of range
	 */
	public void setLatitude(double latitude) throws IllegalArgumentException {
		this.latitude = Filters.filterLongLat(String.valueOf(latitude), Filters.LATITUDE_MAX);
	}

	/**
	 * Returns location name
	 * 
	 * @return location name
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * Sets location name
	 * 
	 * @param locationName new location name
	 * 
	 * @throws IllegalArgumentException
	 * 		if name null, too long, or non-ASCII
	 */
	public void setLocationName(String locationName) throws IllegalArgumentException {
		this.locationName = Filters.filterASCII(locationName);
		// null will already be filtered out
		if(locationName.length() >= MAX_STRING) {
			throw new IllegalArgumentException("Too long name");
		}
	}

	/**
	 * Returns location description
	 * 
	 * @return location description
	 */
	public String getLocationDescription() {
		return locationDescription;
	}

	/**
	 * Sets location description
	 * 
	 * @param locationDescription new location description
	 * 
	 * @throws IllegalArgumentException
	 * 		if description null, too long, or non-ASCII
	 */
	public void setLocationDescription(String locationDescription) throws IllegalArgumentException {
		this.locationDescription = Filters.filterASCII(locationDescription);
		// null will already be filtered out
		if(locationDescription.length() >= MAX_STRING) {
			throw new IllegalArgumentException("Too long description");
		}
	}
	
	@Override
    public String toString() {
        return "(" + longitude + ", " + latitude + ") " + locationName + " - " + locationDescription;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(latitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((locationDescription == null) ? 0 : locationDescription.hashCode());
		result = prime * result + ((locationName == null) ? 0 : locationName.hashCode());
		temp = Double.doubleToLongBits(longitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoTiFiLocation other = (NoTiFiLocation) obj;
		if (Double.doubleToLongBits(latitude) != Double.doubleToLongBits(other.latitude))
			return false;
		if (locationDescription == null) {
			if (other.locationDescription != null)
				return false;
		} else if (!locationDescription.equals(other.locationDescription))
			return false;
		if (locationName == null) {
			if (other.locationName != null)
				return false;
		} else if (!locationName.equals(other.locationName))
			return false;
		if (Double.doubleToLongBits(longitude) != Double.doubleToLongBits(other.longitude))
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
}
