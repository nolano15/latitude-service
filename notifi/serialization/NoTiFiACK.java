/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

/**
 * NoTiFi ACK
 */
public class NoTiFiACK extends NoTiFiMessage {
	// Code for NoTiFi ACK
	public static final int CODE = 5;
	
	/**
	 * Constructs NoTiFi ACK from values
	 * 
	 * @param msgId message ID
	 * 
	 * @throws IllegalArgumentException
	 * 		if message ID invalid
	 */
	public NoTiFiACK(int msgId) throws IllegalArgumentException {
		this.setMsgId(msgId);
	}
	
	@Override
	public byte[] encode() {
		return encodeHeader(HEADER_SIZE, CODE).array();
	}

	@Override
	public int getCode() {
		return CODE;
	}

	@Override
	public int getBytes() {
		return HEADER_SIZE;
	}

	@Override
	public String toString() {
		return "NoTiFiACK []";
	}
}
