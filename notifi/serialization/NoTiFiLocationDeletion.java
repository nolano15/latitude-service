/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

/**
 * NoTiFi location deletion
 */
public class NoTiFiLocationDeletion extends NoTiFiLocationOption {
	// Code for NoTiFi location deletion
	public static final int CODE = 2;
	
	/**
	 * Constructs NoTiFi location deletion from values
	 * 
	 * @param msgId message ID
	 * @param location location to delete
	 * 
	 * @throws IllegalArgumentException
	 * 		if data validation fails
	 */
	public NoTiFiLocationDeletion(int msgId, NoTiFiLocation location) throws IllegalArgumentException {
		this.setMsgId(msgId);
		this.setLocation(location);
	}
	
	@Override
	public int getCode() {
		return CODE;
	}
	
	@Override
	public byte[] encode() {
		return encodeOpt(CODE);
	}
	
	@Override
    public String toString() {
        return "Deletion\n" + location;
    }
}
