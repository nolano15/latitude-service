/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiMessage;
import notifi.serialization.NoTiFiRegister;
import tools.Filters;

/**
 * NoTiFi register test
 */
class NoTiFiRegisterTest {
	// Sample NoTiFi Register packet
	private static final byte[] REG_PKT = new byte[] {0x30, 0x5,
			0x11, (byte)0x94, 0x3E, (byte)0x81, // ip address
			(byte)0x88, 0x13 // port
			};
	
	/**
     *  Contains all basic encoding/decoding tests
     */
    @Nested
    @DisplayName("Basic")
    class BasicTests {
    	/**
    	 * Testing a typical decode
    	 * 
    	 * @throws IllegalArgumentException
    	 * 		if values invalid
    	 * @throws IOException
    	 * 		if I/O problem including packet too long/short (EOFException)
    	 */
    	@Test
    	@DisplayName("Decode")
    	void testDecode() throws IllegalArgumentException, IOException {
    		// test general attr
    		var msg = NoTiFiMessage.decode(REG_PKT);
    		assertAll(
    			() -> assertEquals(5, msg.getMsgId()),
                () -> assertEquals(NoTiFiRegister.CODE, msg.getCode())
            );
    		
    		// cast then test NoTiFiRegister attr
    		var newMsg = (NoTiFiRegister) msg;
    		
    		assertAll(
    			() -> assertEquals("129.62.148.17", newMsg.getAddress().getHostAddress()),
    			() -> assertEquals(5000, newMsg.getPort())
            );
    	}
    	
    	/**
    	 * Testing a typical encode
    	 * 
    	 * @throws IllegalArgumentException
    	 * 		if values invalid
    	 * @throws IOException
    	 * 		if I/O problem including packet too long/short (EOFException)
    	 */
    	@Test
    	@DisplayName("Encode")
    	void testEncode() throws IllegalArgumentException, IOException {
            // immediately encoding the return should be the original packet
            assertArrayEquals(REG_PKT, NoTiFiMessage.decode(REG_PKT).encode());
    	}
    }
    
    /**
     * Contains all invalid tests
     */
    @Nested
    @DisplayName("Invalid Tests")
    class InvalidTests {
    	// Test packets with bad versions/codes
    	@Test
    	@DisplayName("Decoding")
        void testInvalidDecode() {
    		assertThrows(IOException.class,
				() -> NoTiFiMessage.decode(new byte[] {0x30, 0x5,
						0x11, (byte)0x94, 0x3E, (byte)0x81, // ip address
						(byte)0x88
						}), "Short packet");
        }

    	// Test null and out-of-bounds args
    	@Test
    	@DisplayName("Field tests")
        void testInvalidFields() throws UnknownHostException {
    		// init vanilla NoTiFiRegister
    		var ntfReg = new NoTiFiRegister(5, (Inet4Address)Inet4Address.getByName("129.62.148.17"), 5000);
    		assertAll(
    			() -> assertThrows(IllegalArgumentException.class,
    					() -> ntfReg.setAddress(null), "Null IP address"),
    			() -> assertThrows(IllegalArgumentException.class,
    					() -> ntfReg.setPort( (int)Math.pow(2, Filters.PORT_MAX_POWER) ), "Invalid port"),
    			() -> assertThrows(IllegalArgumentException.class,
    					() -> ntfReg.setPort(-1), "Invalid port")
            );
        }
    }
}
