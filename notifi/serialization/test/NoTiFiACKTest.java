/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiACK;
import notifi.serialization.NoTiFiMessage;

/**
 * NoTiFi ACK test
 */
class NoTiFiACKTest {
	// Sample NoTiFi ACK packet
	private static final byte[] ACK_PKT = new byte[] {0x35, 0x5};

	/**
	 * Testing a typical decode
	 * 
	 * @throws IllegalArgumentException
	 * 		if values invalid
	 * @throws IOException
	 * 		if I/O problem including packet too long/short (EOFException)
	 */
	@Test
	@DisplayName("Decode")
	void testDecode() throws IllegalArgumentException, IOException {
		var msg = NoTiFiMessage.decode(ACK_PKT);	
		assertAll(
			() -> assertEquals(5, msg.getMsgId()),
            () -> assertEquals(NoTiFiACK.CODE, msg.getCode())
        );
	}
	
	/**
	 * Testing a typical encode
	 * 
	 * @throws IllegalArgumentException
	 * 		if values invalid
	 * @throws IOException
	 * 		if I/O problem including packet too long/short (EOFException)
	 */
	@Test
	@DisplayName("Encode")
	void testEncode() throws IllegalArgumentException, IOException {
        // immediately encoding the return should be the original packet
        assertArrayEquals(ACK_PKT, NoTiFiMessage.decode(ACK_PKT).encode());
	}
	
	// Test packets with bad versions/codes
	@Test
	@DisplayName("Versions/Codes")
    void testVCode() {
		assertAll(
			() -> assertThrows(IllegalArgumentException.class,
					() -> NoTiFiMessage.decode(new byte[] {0x25, 0x5}), "Old version"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> NoTiFiMessage.decode(new byte[] {0x36, 0x5}), "Unexpected code"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> NoTiFiMessage.decode(new byte[] {0x3, 0x5}), "Missing code"),
			() -> assertThrows(IOException.class,
					() -> NoTiFiMessage.decode(new byte[0]), "Empty byte array"),
			() -> assertThrows(IOException.class,
					() -> NoTiFiMessage.decode(new byte[] {0x35, 0x0, 0x4}), "Large Packet")
        );
    }
}
