/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import notifi.serialization.NoTiFiLocation;
import tools.Filters;

/**
 * NoTiFi location test
 */
class NoTiFiLocationTest {
	// Test null and out-of-bounds args
	@Test
	@DisplayName("Field tests")
    void testInvalidFields() {
		// init vanilla NoTiFiLocation
		var ntfLoc = new NoTiFiLocation(1, 1.2, 3.4, "BU", "Baylor");
		assertAll(
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setUserId( (long)Math.pow(2, Filters.UNSIGNED_INT_MAX_POWER) ), "Invalid userID"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setUserId(-1L), "Negative userID"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setLongitude(Filters.LONGITUDE_MAX + 1), "Invalid longitude"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setLongitude((Filters.LONGITUDE_MAX + 1)*-1), "Invalid longitude"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setLatitude(Filters.LATITUDE_MAX + 1), "Invalid latitude"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setLatitude((Filters.LATITUDE_MAX + 1)*-1), "Invalid latitude"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setLocationName("�"), "non-ASCII location name"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setLocationDescription("�"), "non-ASCII location description"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setLocationName(null), "null location name"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfLoc.setLocationDescription(null), "null location description")
        );
    }
}
