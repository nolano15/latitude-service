/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import notifi.serialization.NoTiFiError;
import notifi.serialization.NoTiFiMessage;

/**
 * NoTiFi error test
 */
class NoTiFiErrorTest {
	// Sample NoTiFi Error packet
	private static final byte[] ERROR_PKT = new byte[] {0x34, 0x5, 'Y', 'u', 'r', 'p'};

	/**
	 * Testing a typical decode
	 * 
	 * @throws IllegalArgumentException
	 * 		if values invalid
	 * @throws IOException
	 * 		if I/O problem including packet too long/short (EOFException)
	 */
	@Test
	@DisplayName("Decode")
	void testDecode() throws IllegalArgumentException, IOException {
		// test general attr
		var msg = NoTiFiMessage.decode(ERROR_PKT);	
		assertAll(
			() -> assertEquals(5, msg.getMsgId()),
            () -> assertEquals(NoTiFiError.CODE, msg.getCode())
        );
		
		// cast then test NoTiFiError attr
		var newMsg = (NoTiFiError) msg;
		assertEquals("Yurp", newMsg.getErrorMessage());
	}
	
	/**
	 * Testing a typical encode
	 * 
	 * @throws IllegalArgumentException
	 * 		if values invalid
	 * @throws IOException
	 * 		if I/O problem including packet too long/short (EOFException)
	 */
	@Test
	@DisplayName("Encode")
	void testEncode() throws IllegalArgumentException, IOException {
        // immediately encoding the return should be the original packet
        assertArrayEquals(ERROR_PKT, NoTiFiMessage.decode(ERROR_PKT).encode());
	}

	// Test null and out-of-bounds args
	@Test
	@DisplayName("Invalid fields")
    void testInvalidFields() {
		// init vanilla NoTiFiError
		var ntfErr = new NoTiFiError(5, "Yurp");
		assertAll(
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfErr.setMsgId( (int)Math.pow(2, 8) ), "Invalid message ID"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfErr.setErrorMessage("�"), "non-ASCII error message"),
			() -> assertThrows(IllegalArgumentException.class,
					() -> ntfErr.setErrorMessage(null), "Null error message")
        );
    }
}
