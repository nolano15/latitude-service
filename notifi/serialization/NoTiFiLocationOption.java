/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

package notifi.serialization;

import java.util.Objects;

/**
 * Superclass for Addition/Deletion NoTiFiLocationMessages
 */
public abstract class NoTiFiLocationOption extends NoTiFiMessage {
	protected NoTiFiLocation location;
	
	/**
	 * Serializes LocationAddition/Deletion
	 * 
	 * @param code specific NoTiFiMessage code
	 * 
	 * @return serialized message bytes
	 */
	protected byte[] encodeOpt(int code) {
		// packet memory allocation
		return encodeHeader(getBytes(), code)
				.put( location.encode() ) // add Location
				.array();
	}
	
	@Override
	public int getBytes() {
		return HEADER_SIZE + location.getBytes();
	}
	
	/**
	 * Get location
	 * 
	 * @return location
	 */
	public NoTiFiLocation getLocation() {
		return location;
	}

	/**
	 * Set location
	 * 
	 * @param location location to add/delete
	 * 
	 * @throws IllegalArgumentException
	 * 		 if location is null
	 */
	public void setLocation(NoTiFiLocation location) throws IllegalArgumentException {
		try {
			this.location = Objects.requireNonNull(location);
		} catch(NullPointerException e) {
			throw new IllegalArgumentException("null location", e);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoTiFiLocationOption other = (NoTiFiLocationOption) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NoTiFiLocationOption [location=" + location + "]";
	}
}
