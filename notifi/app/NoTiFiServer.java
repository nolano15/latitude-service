/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 6
* Class: CS 4321
*
************************************************/

package notifi.app;

import java.io.IOException;
import java.net.*;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import latitude.app.LatitudeSuperServer;
import mapservice.Location;
import mapservice.MapManager;
import mapservice.MapObserver;
import notifi.serialization.*;

/**
 * UDP server that sends and receives NoTiFiMessages to/from registered Clients
 */
public class NoTiFiServer extends LatitudeSuperServer implements MapObserver, Runnable {
	private Set<Location> currLocs;
	private Set<Client> regClients;
	private DatagramSocket socket;
	
	/**
	 * Constructs Runnable UDP NoTiFiServer
	 * 
	 * @param mgr reference to MapManager to register with
	 * @param servPort port that this NoTiFiServer will Run on
	 * 
	 * @throws SocketException 
	 * 		if DatagramSocket cannot be opened
	 */
	public NoTiFiServer(MapManager mgr, int servPort) throws SocketException {
		// save reference
		super(mgr);
		
		// update() will be called whenever there is an addition/deletion
    	mgr.register(this);
    	
    	// copy current Locations
    	currLocs = new HashSet<Location>( mgr.getLocations() );
    	
    	regClients = new HashSet<Client>();
    	socket = new DatagramSocket(servPort);
	}
	
	/**
	 * Handle received NoTiFiMessages, as well as registering/deregistering clients,
	 * and sending ACKs.
	 */
	@Override
	public void run() {
		try {
			while(true) {
				receive(socket, new HandleStrategy() {
					@Override
					public void handle(DatagramPacket receivePacket) throws IOException {
						// log client
		                logger.info("Servicing client at " + receivePacket.getSocketAddress());

		                NoTiFiMessage toSend; // NoTiFiMessage to send to client
		                try {
		                	// NoTiFiMessage from client
					        var receivedNTF = NoTiFiMessage.decode( receivePacket.getData() );
					        
					        String msg = null;
					        boolean unknown = false; // will check receipt of unknown codes
					        // prospective client
			        		var client = new Client((Inet4Address)receivePacket.getAddress(), receivePacket.getPort());
			        		
					        switch( receivedNTF.getCode() ) {
					        	case NoTiFiRegister.CODE: msg = filterReg((NoTiFiRegister)receivedNTF, client);
					        		// if no error msg
					        		if(msg == null) {
					        			// then register client
					        			regClients.add(client);
					        		}
					        		break;
					        	case NoTiFiDeregister.CODE:
					        		// if client is not registered
					        		if( !regClients.contains(client) ) {
					        			msg = "Unknown client";
					        		} else { // deregister client
					        			regClients.remove(client);
					        		}
					        		break;
					        	case NoTiFiACK.CODE: // waterfall
					        	case NoTiFiError.CODE:
					        	case NoTiFiLocationAddition.CODE:
					        	case NoTiFiLocationDeletion.CODE: msg = "Unexpected message type: " + receivedNTF.getCode();
					        		break;
					        	default: msg = "Unknown code: " + receivedNTF.getCode();
					        		unknown = true;
					        }
					        
					        if(msg == null) { // if no errors, send ACK
					        	toSend = new NoTiFiACK( receivedNTF.getMsgId() );
					        } else {
					        	// msgID of zero if unknown code was received
					        	toSend = new NoTiFiError(unknown ? 0 : receivedNTF.getMsgId(), msg);
					        }
					        
		                } catch(Exception e) {
		                	toSend = new NoTiFiError(0, "Unable to parse message");
		                }
		                var sendPacket = new DatagramPacket(toSend.encode(),  // Sending packet
		                		toSend.getBytes(), receivePacket.getAddress(), receivePacket.getPort());
		                socket.send(sendPacket);          // Send server response
					}
				});
			}
		} catch (IOException e) {
			// Server shuts down if socket error
		}
	}

	/**
	 * Detects which Location was updated and notifies all registered clients
	 */
	@Override
	public void update() {
		// copy new Locations
		var newLocs = new HashSet<Location>( mgr.getLocations() );

		Location addLoc = null;
		// if a Location was updated
		if(newLocs.size() <= currLocs.size()) {
			
			for(Location currLoc : currLocs) {
				
				String currName = currLoc.getName().split(":")[0];
				
				for(Location newLoc : newLocs) {
					// if names match
					if( currName.equals( newLoc.getName().split(":")[0] ) ) {
						// if Locations don't match, then this client updated
						if( !equalsLoc(currLoc, newLoc) ) {
							// send deletion
							sendUpdate(new NoTiFiLocationDeletion(getRandomId(), convertLocNTF(currLoc)));
							// Location to add
							addLoc = newLoc;
							break; // break loop
						}
					}
				}
			}
		} else { // new Location was added
			addLoc = getNewLoc(newLocs, currLocs);
		}
		// always send addition update
		sendUpdate(new NoTiFiLocationAddition(getRandomId(), convertLocNTF(addLoc)));
		
		// update to new Locations
		currLocs = newLocs;
	}
	
	/**
	 * Sends update to all clients
	 * 
	 * @param updateNTF message to send to registered clients
	 */
	private void sendUpdate(NoTiFiLocationOption updateNTF) {
		regClients.parallelStream().forEach(c -> {
			var sendPacket = new DatagramPacket(updateNTF.encode(),  // Sending packet
					updateNTF.getBytes(), c.getIp(), c.getPort());
            try {
            	socket.send(sendPacket);          // Send server response
            } catch(IOException e) {
            	// UDP server failed to send packet
            }
		});
	}
	
	/**
	 * Finds the difference between two Sets and returns it
	 * 
	 * @param bigger Set with larger size
	 * @param smaller Set with smaller size
	 * 
	 * @return new Location that has been added
	 */
	private Location getNewLoc(Set<Location> bigger, Set<Location> smaller) {
		var diff = new HashSet<Location>(bigger);
		diff.removeAll(smaller); // only updated Location will remain
		return diff.iterator().next(); // return only element
	}
	
	/**
	 * Filters prospective client through specified Server Protocol
	 * 
	 * @param reg NoTiFiRegister sent by client
	 * @param client prospective client
	 * 
	 * @return error msg, if any
	 */
	private String filterReg(NoTiFiRegister reg, Client client) {
		return reg.getAddress().isMulticastAddress() ?
				"Bad address" : regClients.contains(client) ?
						"Already registered" : reg.getPort() != client.getPort() ?
								"Incorrect port" : null;
	}
	
	/**
	 * Receives a packet from DatagramSocket and handles
	 * 
	 * @param socket DatagramSocket for sending and receiving packets
	 * @param hstrat unique strategy for handling packets
	 * 
	 * @throws IOException
	 * 		if issues communicating with server
	 */
	public static void receive(DatagramSocket socket, HandleStrategy hstrat) throws IOException {
		var receivePacket =  							   // Receiving packet
	        new DatagramPacket(new byte[BUFSIZE], BUFSIZE);
		
		if(socket instanceof MulticastSocket) {
			((MulticastSocket)socket).receive(receivePacket); // Allow Multicast
		} else {
			socket.receive(receivePacket);  // Attempt NoTiFiMessage reception
		}
    	
    	try {
    		// handle received packet with custom handling strategy
    		hstrat.handle(receivePacket);
    	} catch(Exception e) {
    		System.out.println("Unable to parse message: " + e.getMessage());
    	}
	}
	
	/**
     * Converts a Location to a NoTiFiLocation
     * 
     * @param location Location to convert
     * 
     * @returns converted NoTiFiLocation
     */
    private NoTiFiLocation convertLocNTF(Location location) {
        return new NoTiFiLocation(getUserId(location), Double.parseDouble( location.getLongitude() ), Double.parseDouble( location.getLatitude() ),
                location.getName(), location.getDescription());
    }
    
    /**
     * Compares equality of a pair of Locations
     * 
     * @param a first Location
     * @param b second Location
     * 
     * @return equality of both Locations
     */
    private boolean equalsLoc(Location a, Location b) {
    	return !a.getName().equals( b.getName() ) ?
    			false : !a.getDescription().equals( b.getDescription() ) ?
    					false : !a.getLatitude().equals( b.getLatitude() ) ?
    							false : !a.getLongitude().equals( b.getLongitude() ) ?
    									false : true;
    }
	
	/**
	 * Get random ID
	 * 
	 * @return random ID
	 */
	public static int getRandomId() {
		// create random non-negative number less than 2^8, max msgID
		return ThreadLocalRandom.current().nextInt(0, (int)Math.pow(2, 8));
	}
}

/**
 * Strategy design pattern for handling server responses
 */
interface HandleStrategy {
	/**
	 * Method that defines how received packets should be handled
	 * 
	 * @param receivePacket packet received from server
	 * 
	 * @throws IOException
	 * 		if issues communicating with server
	 */
	default void handle(DatagramPacket receivePacket) throws IOException {
	}
}

