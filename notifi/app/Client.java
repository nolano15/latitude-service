/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 6
* Class: CS 4321
*
************************************************/

package notifi.app;

import java.net.Inet4Address;

/**
 * Struct to hold an IP address and port
 */
public class Client {
	private Inet4Address ip;
	private int port;
	
	/**
	 * Constructs Client
	 * 
	 * @param ip IP address used by client
	 * @param port port used by client
	 */
	public Client(Inet4Address ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	/**
	 * Get IP address
	 * 
	 * @return IP address
	 */
	public Inet4Address getIp() {
		return ip;
	}

	/**
	 * Get port
	 * 
	 * @return port
	 */
	public int getPort() {
		return port;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result + port;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (port != other.port)
			return false;
		return true;
	}
}
