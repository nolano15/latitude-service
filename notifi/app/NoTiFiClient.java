/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 5
* Class: CS 4321
*
************************************************/

package notifi.app;

import java.io.*;
import java.net.*;
import notifi.serialization.*;

/**
 * UDP client that sends and receives NoTiFiMessages
 */
public class NoTiFiClient extends NoTiFiSuperClient {
	private static final int MAXTRIES = 1;     // Maximum retransmissions
	
	// Global command line args
	private static InetAddress serverAddress, registerAddress;

	/**
     * Defines Client interface with the NoTiFi service
     * 
     * @param args server, port #, and local IP
     * 
	 * @throws IOException
	 * 		if server cannot be resolved
     */
	public static void main(String[] args) throws IOException {
		if (args.length != 3) { // Test for correct # of args
	      System.err.print("Parameter(s): <Server> <Port> <Register IP>");
          System.exit(0); // terminate
	    }
	    serverAddress = InetAddress.getByName(args[0]);   // Server address
		registerAddress = InetAddress.getByName(args[2]); // Register address

	    servPort = Integer.parseInt(args[1]);

	    try(var socket = new DatagramSocket()) {
	    	
	    	socket.setSoTimeout(TIMEOUT);  // Maximum receive blocking time (milliseconds)
	    	
	    	// Register
	    	sendReg(socket, NoTiFiRegister.CODE);
	    	
	    	// Now can receive NoTiFiLocation updates
		    update(socket);
		    
		    // Deregister
		    sendReg(socket, NoTiFiDeregister.CODE);
		    
	    } catch (IOException e) { // handle server comm probs
            System.err.print("Unable to communicate: " + e.getMessage());
            System.exit(0); // terminate
        }
	}
	
	/**
	 * Handles setup and creation of NoTiFiRegisterOption
	 * 
	 * @param socket DatagramSocket for sending and receiving packets
	 * @param registerAddress input IP to register/deregister
	 * @param code which NoTiFiRegisterOption to instantiate
	 * 
	 * @throws IOException
	 * 		if issues communicating with server
	 */
	private static void sendReg(DatagramSocket socket, int code) throws IOException {
		NoTiFiRegisterOption regopt = null;
		
		switch(code) {
			case NoTiFiRegister.CODE: regopt = new NoTiFiRegister(NoTiFiServer.getRandomId(),
					(Inet4Address)registerAddress, socket.getLocalPort());
				break;
			case NoTiFiDeregister.CODE: regopt = new NoTiFiDeregister(NoTiFiServer.getRandomId(),
					(Inet4Address)registerAddress, socket.getLocalPort());
		}
    	
	    // Register msg to send to server
	    waitACK(socket, regopt);
	}
	
	// static declaration for compatibility with anonymous class
	static boolean receivedACK;

	/**
	 * Registers/Deregisters client and waits for corresponding ACK
	 * 
	 * @param socket DatagramSocket for sending and receiving packets
	 * @param reg NoTiFiRegisterOption with input IP and client's local port
	 * 
	 * @throws IOException
	 * 		if issues communicating with server
	 */
	private static void waitACK(DatagramSocket socket, NoTiFiRegisterOption reg) throws IOException {
		var sendPacket = new DatagramPacket(reg.encode(),  // Sending packet
		        reg.getBytes(), serverAddress, servPort);

	    int tries = 0;      // Packets may be lost, so we have to keep trying
	    receivedACK = false;
	    do {
	      socket.send(sendPacket);          // Send NoTiFiRegisterOption
	      try {
	    	  NoTiFiServer.receive(socket, new HandleStrategy() {
					@Override
					public void handle(DatagramPacket receivePacket) throws IOException {
						// NoTiFiMessage from server
				        var receivedNTF = NoTiFiMessage.decode( receivePacket.getData() );
				        
				        // test if ACK
				        if(receivedNTF.getCode() == NoTiFiACK.CODE) {
				        	// test if correct ACK
				        	if(receivedNTF.getMsgId() == reg.getMsgId()) {
				        		receivedACK = true;
				        	} else {
				        		System.out.println("Unexpected MSG ID");
				        	}
				        } else { // else handle according to the Client Protocol
				        	handleNoTiFiClient(receivedNTF);
				        }
					}
				});
	      } catch (InterruptedIOException e) {  // We did not get anything
	        tries++;
	      }
	    } while ((!receivedACK) && (tries <= MAXTRIES));

	    // if ACK was never received
	    if (!receivedACK) {
	    	System.out.println("Unable to " +
	    			(reg.getCode() == NoTiFiDeregister.CODE ? "de" : "") // add "de" if deregistering
	    			+ "register");
	    	System.exit(0); // terminate
	    }
	}
}
