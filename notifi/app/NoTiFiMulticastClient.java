/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 8
* Class: CS 4321
*
************************************************/

package notifi.app;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * UDP client that receives NoTiFiLocation updates
 */
public class NoTiFiMulticastClient extends NoTiFiSuperClient {

	/**
	 * Defines MulticastClient interface with the NoTiFi service
	 * 
	 * @param args Multicast addr and port
	 * 
	 * @throws IOException
	 * 		if Multicast server cannot be resolved
	 */
	public static void main(String[] args) throws IOException {
		if (args.length != 2) { // Test for correct # of args
	      System.err.print("Parameter(s): <Multicast Address> <Port>");
          System.exit(0); // terminate
	    }
		
		InetAddress multicastAddress = InetAddress.getByName(args[0]);   // Multicast address
		servPort = Integer.parseInt(args[1]);
		
		if ( !multicastAddress.isMulticastAddress() ) {  // Test if multicast address
			throw new IllegalArgumentException("Not a multicast address");
		}
	    
	    try(var multicastSocket = new MulticastSocket(servPort)) {
	   
	    	multicastSocket.setSoTimeout(TIMEOUT);  // Maximum receive blocking time (milliseconds)
	    	
	    	// join the NoTiFi Multicast group
	    	multicastSocket.joinGroup(multicastAddress);
	    	
	    	// Now can receive NoTiFiLocation updates
	    	update(multicastSocket);

	    	// Leave Multicast group
			multicastSocket.leaveGroup(multicastAddress);
			
	    } catch (IOException e) { // handle server comm probs
            System.err.print("Unable to communicate: " + e.getMessage());
            System.exit(0); // terminate
        }
	}
}
