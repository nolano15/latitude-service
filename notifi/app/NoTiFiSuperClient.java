/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 8
* Class: CS 4321
*
************************************************/

package notifi.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.util.Scanner;

import notifi.serialization.*;

/**
 * NoTiFiSuperClient will define how NoTiFi clients handle updates and when to quit
 */
public abstract class NoTiFiSuperClient implements Runnable {
	protected static final int TIMEOUT = 3000;   // Resend timeout (milliseconds)
	
	protected static int servPort;
	
	/**
	 * Receive NoTiFi updates until Thread monitoring user input detects "quit"
	 * 
	 * @param socket UDP socket to receive updates from
	 * 
	 * @throws IOException
	 * 		if issues communicating with server
	 */
	protected static void update(DatagramSocket socket) throws IOException {
		// Start user quitWatch
	    new Thread(new NoTiFiMulticastClient()).start();
	    
	    // while quitWatch thread is open
	    while(Thread.activeCount() > 1) {
	    	// continually receive packets
	    	try {
	    		NoTiFiServer.receive(socket, new HandleStrategy() {
					@Override
					public void handle(DatagramPacket receivePacket) throws IOException {
						// Decode and handle
				    	handleNoTiFiClient( NoTiFiMessage.decode( receivePacket.getData() ) );
					}
				});
	    	} catch(SocketTimeoutException e) {
	    		// Socket timed out, which is fine, but now check to see if user wants to quit
	    	}
	    }
	}
	
	/**
	 * Separate thread to handle user input, will run forever until quit is entered
	 */
	@Override
	public void run() {
		// wrap standard in
	    try(var userIn = new Scanner(new BufferedReader(new InputStreamReader(System.in)))) {
	    	while(true) {
	    		// run until user types quit and enter
	        	if( "quit".equals( userIn.next() ) ) {
	        		break;
	        	}
	    	}
	    }
	}
	
	/**
	 * Client Protocol for handling NoTiFiMessages
	 * 
	 * @param ntf NoTiFiMessage to handle
	 */
	protected static void handleNoTiFiClient(NoTiFiMessage ntf) {
		String message;
		switch( ntf.getCode() ) { // "" + is used to call toString
			case NoTiFiLocationAddition.CODE: message = "" + (NoTiFiLocationAddition)ntf;
				break;
			case NoTiFiLocationDeletion.CODE: message = "" + (NoTiFiLocationDeletion)ntf;
				break;
			case NoTiFiError.CODE: message = "" + (NoTiFiError)ntf;
				break;
			default: message = "Unexpected message type";
		}
		System.out.println(message);
	}
}
