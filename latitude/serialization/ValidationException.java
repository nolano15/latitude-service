/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 0
* Class: CS 4321
*
************************************************/

package latitude.serialization;


/**
 * Exception for validation
 */
public class ValidationException extends Exception {
    private static final long serialVersionUID = -8359463875150982348L;
    
    private String invalidToken;

    /**
     * Constructs validation exception
     * 
     * @param invalidToken token that failed validation
     * @param message exception message
     * @param cause exception cause
     */
    public ValidationException(String invalidToken, String message, Throwable cause) {
        super(message, cause);
        this.invalidToken = invalidToken;
    }

    /**
     * Constructs validation exception with null cause
     * 
     * @param invalidToken token that failed validation
     * @param message exception message
     */
    public ValidationException(String invalidToken, String message) {
        this(invalidToken, message, null);
    }
    
    /**
     * Get token that failed validation
     * 
     * @returns token that failed validation
     */
    public String getInvalidToken() {
        return invalidToken;
    }
}
