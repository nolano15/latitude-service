/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 1
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import java.io.IOException;
import java.util.Objects;

/**
 * Represents a new location and provides serialization/deserialization
 */
public class LatitudeNewLocation extends LatitudeMessage {
	// Operation for Latitude New Location
	public static final String OPERATION = "NEW";
    private LocationRecord location;
    
    /**
     * Constructs new location using set values
     * 
     * @param mapId ID for message map
     * @param location new location
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public LatitudeNewLocation(long mapId, LocationRecord location) throws ValidationException {
        setMapId(mapId);
        this.setLocationRecord(location);
    }

    protected void encodeSwitch(MessageOutput out) throws IOException {
        // encode new LocationRecord
        location.encode(out);
    }
    
    @Override
	public String getOperation() {
		return OPERATION;
	}

    /**
     * Returns location
     * 
     * @returns location record
     */
    public final LocationRecord getLocationRecord() {
        return location;
    }

    /**
     * Sets location
     * 
     * @param location new location
     * 
     * @throws ValidationException
     *      if null error message
     */
    public final void setLocationRecord(LocationRecord location) throws ValidationException {
        try {
            this.location = new LocationRecord( Objects.requireNonNull(location, "null location") );
        } catch(NullPointerException e) {
            throw new ValidationException(null, "cannot set to null location", e);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof LatitudeNewLocation))
            return false;
        LatitudeNewLocation other = (LatitudeNewLocation) obj;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "LatitudeNewLocation [location=" + location + "]";
    }
}
