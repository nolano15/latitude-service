/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 7
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import tools.BinaryTools;

/**
 * Non-blocking I/O deframer for message. The message delimiter is the given byte sequence
 */
public class MessageNIODeframer {
	private byte[] currMsg, delimiter;
	
	/**
	 * Create message deframer with specified byte sequence delimiter
	 * 
	 * @param delimiter bytes of message delimiter
	 */
	public MessageNIODeframer(byte[] delimiter) {
		this.delimiter = delimiter;
		this.currMsg = new byte[0]; // init
	}
	
	/**
	 * Decode the next message (if available)
	 * 
	 * @param buffer next bytes of message
	 * 
	 * @return deframed message or null if frame incomplete
	 * 
	 * @throws NullPointerException
	 * 		if buffer is null
	 */
	public byte[] nextMsg(byte[] buffer) {
		boolean fullMsg = false;
		
		// iterate over buffer until fullMsg is found
		for(int i = 0; i < buffer.length && !fullMsg; i++) {
			if(buffer[i] == delimiter[0]) {
				// assumed true unless delimiter iteration proves ow
				fullMsg = true;
				
				for(int j = 1; j < delimiter.length; j++) {
					i++;
					if(buffer[i] != delimiter[j]) {
						fullMsg = false;
						j = delimiter.length; // break
					}
				}
			}
		}
		
		currMsg = BinaryTools.concat(currMsg, buffer);
		
		// if fullMsg is true, then return msg, else return null
		return fullMsg ? currMsg : null;
	}
}
