/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 1
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import java.io.IOException;

/**
 * Represents an location request and provides serialization/deserialization
 */
public class LatitudeLocationRequest extends LatitudeMessage {
	// Operation for Latitude Location Request
	public static final String OPERATION = "ALL";
	
    /**
     * Constructs location request using set values
     * 
     * @param mapId ID for message map
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public LatitudeLocationRequest(long mapId) throws ValidationException {
        setMapId(mapId);
    }

    // empty implementation
    protected void encodeSwitch(MessageOutput out) throws IOException {}
    
    @Override
	public String getOperation() {
		return OPERATION;
	}

    @Override
    public String toString() {
        return "LatitudeLocationRequest []";
    }
}
