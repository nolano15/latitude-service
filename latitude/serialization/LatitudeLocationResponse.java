/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 1
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import tools.Filters;

/**
 * Represents a location response message and provides serialization/deserialization
 */
public class LatitudeLocationResponse extends LatitudeMessage {
	// Operation for Latitude Location Response
	public static final String OPERATION = "RESPONSE";
    private String mapName;
    private List<LocationRecord> mapLocations;
    
    /**
     * Constructs location response using set values
     * 
     * @param mapId ID for message map
     * @param mapName name of location
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public LatitudeLocationResponse(long mapId, String mapName) throws ValidationException {
        setMapId(mapId);
        setMapName(mapName);
        
        this.mapLocations = new ArrayList<LocationRecord>();
    }
    
    protected void encodeSwitch(MessageOutput out) throws IOException {
        // encode mapName and list size
        LatitudeProtocol.writeLatString(out, mapName);
        out.write(mapLocations.size() + " ");

        // for each LocationRecord, encode it
        for(LocationRecord locRec : mapLocations) {
            locRec.encode(out);
        }
    }
    
    /**
     * Decodes a list of LocationRecords
     * 
     * @param in deserialization input source
     * 
     * @returns LatitudeLocationResponse with deserialized LocationRecords
     * 
     * @throws ValidationException
     *      if validation failure
     * @throws IOException
     *      if I/O problem
     */
    protected LatitudeLocationResponse decodeLocs(MessageInput in) throws ValidationException, IOException {
         // n is how many LocationRecords to decode
         int n = (int)LatitudeProtocol.readId(in);
         
         for(int i = 0; i < n; i++) {
             addLocationRecord(new LocationRecord(in));
         }
         return this;
    }
    
    @Override
	public String getOperation() {
		return OPERATION;
	}

    /**
     * Returns map name
     * 
     * @returns map name
     */
    public String getMapName() {
        return mapName;
    }

    /**
     * Sets map name
     * 
     * @param mapName new name
     * 
     * @throws ValidationException
     *      if validation fails (including mapName is null)
     */
    public final void setMapName(String mapName) throws ValidationException {
    	try {
        	this.mapName = Filters.filterASCII(mapName);
        } catch(IllegalArgumentException e) {
        	throw new ValidationException(mapName, "Invalid map name", e);
        }
    }

    /**
     * Returns list of map locations
     * 
     * @returns map locations
     */
    public List<LocationRecord> getLocationRecordList() {
        // encapsulation
        return new ArrayList<LocationRecord>(mapLocations);
    }

    /**
     * Adds new location
     * 
     * @param location new location to add
     * 
     * @throws ValidationException
     *      if location is null
     */
    public void addLocationRecord(LocationRecord location) throws ValidationException {
        try {
            this.mapLocations.add( Objects.requireNonNull(location, "null location") );
        } catch(NullPointerException e) {
            throw new ValidationException(null, "cannot add null LocationRecord", e);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((mapLocations == null) ? 0 : mapLocations.hashCode());
        result = prime * result + ((mapName == null) ? 0 : mapName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof LatitudeLocationResponse))
            return false;
        LatitudeLocationResponse other = (LatitudeLocationResponse) obj;
        if (mapLocations == null) {
            if (other.mapLocations != null)
                return false;
        } else if (!mapLocations.equals(other.mapLocations))
            return false;
        if (mapName == null) {
            if (other.mapName != null)
                return false;
        } else if (!mapName.equals(other.mapName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "mapID=" + mapId + " - LocationResponse" +
                mapLocations.parallelStream()
                    .map(lr -> "\n" + lr.toString()) // convert each LocationRecord with toString() on its own line
                    .collect( Collectors.joining() );
    }
}
