/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 1
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import java.io.IOException;

import tools.Filters;

/**
 * Represents an error message and provides serialization/deserialization
 */
public class LatitudeError extends LatitudeMessage {
	// Operation for Latitude Error
	public static final String OPERATION = "ERROR";
    private String errorMessage;
    
    /**
     * Constructs error message using set values
     * 
     * @param mapId ID for message map
     * @param errorMessage  error message
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public LatitudeError(long mapId, String errorMessage) throws ValidationException {
        setMapId(mapId);
        setErrorMessage(errorMessage);
    }
    
    protected void encodeSwitch(MessageOutput out) throws IOException {
        // encode errorMessage
        LatitudeProtocol.writeLatString(out, errorMessage);
    }
    
    @Override
	public String getOperation() {
		return OPERATION;
	}

    /**
     * Return error message
     * 
     * @returns error message
     */
    public final String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Set error message
     * 
     * @param errorMessage new error message
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public final void setErrorMessage(String errorMessage) throws ValidationException {
    	try {
        	this.errorMessage = Filters.filterASCII(errorMessage);
        } catch(IllegalArgumentException e) {
        	throw new ValidationException(errorMessage, "Invalid error message", e);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((errorMessage == null) ? 0 : errorMessage.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof LatitudeError))
            return false;
        LatitudeError other = (LatitudeError) obj;
        if (errorMessage == null) {
            if (other.errorMessage != null)
                return false;
        } else if (!errorMessage.equals(other.errorMessage))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Error: " + errorMessage;
    }
}
