/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 3
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import java.io.IOException;

import tools.Filters;

/**
 * A struct of static helper functions and constants designed for the LatitudeProtocol
 */
public class LatitudeProtocol {
	
    /**
     * Reads and parses long id
     * 
     * @param in deserialization input source
     * 
     * @returns parsed long
     * 
     * @throws ValidationException
     *      if validation failure
     * @throws IOException
     *      if I/O problem
     */
    public static long readId(MessageInput in) throws ValidationException, IOException {
        if(in == null) {
            throw new NullPointerException("null MessageInput");
        }
        
        String token = in.read();
        long id;

        try {
            id = Long.parseLong( Filters.filterIo(token) );
        } catch (NumberFormatException e) {
            throw new ValidationException(token, "NaN id", e);
        }
        
        return id;
    }
    
    /**
     * Read Latitude protocol char count + string
     * 
     * @param in deserialization input source
     * 
     * @returns decoded String
     * 
     * @throws ValidationException
     *      if validation failure
     * @throws IOException
     *      if I/O problem
     */
    public static String readLatString(MessageInput in) throws ValidationException, IOException {
        if(in == null) {
            throw new IOException("null MessageInput");
        }
        
        return Filters.filterIo( in.read( (int)readId(in) ) );
    }
    
    /**
     * Writes a String according to the LatitudeProtocol
     * 
     * @param out byte output source
     * @param message String to be encoded
     * 
     * @returns out for fluid interfaces
     * 
     * @throws IOException
     *      if I/O problem
     */
    public static void writeLatString(MessageOutput out, String message) throws IOException {
        if(out == null) {
            throw new IOException("null MessageOutput");
        }
        
        out.write(message.length() + " " + message);
    }
}
