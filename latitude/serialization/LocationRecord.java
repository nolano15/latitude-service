/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 0
* Class: CS 4321
*
************************************************/

package latitude.serialization;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

import tools.Filters;

/**
 * Represents a location record and provides serialization/deserialization
 */
public class LocationRecord {
    private long userId;
    private String longitude, latitude, locationName, locationDescription;
    
    /**
     * Constructs location record with set values
     * 
     * @param userId ID for user
     * @param longitude position of location
     * @param latitude position of location
     * @param locationName name of location
     * @param locationDescription description of location
     *
     * @throws ValidationException
     *      if validation fails
     */
    public LocationRecord(long userId, String longitude, String latitude, String locationName, String locationDescription)
            throws ValidationException {
        this.setUserId(userId);      
        this.setLongitude(longitude);
        this.setLatitude(latitude);
        this.setLocationName(locationName);
        this.setLocationDescription(locationDescription);
    }
    
    /**
     * Copy Constructor
     * 
     * @param that LocationRecord to copy to this
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public LocationRecord(LocationRecord that) throws ValidationException {
        this(that.getUserId(), that.getLongitude(), that.getLatitude(), that.getLocationName(), that.getLocationDescription());
    }

    /**
     * Constructs location record using deserialization
     * 
     * @param in deserialization input source
     *
     * @throws ValidationException
     *      if validation failure
     * @throws IOException
     *      if I/O problem
     */
    public LocationRecord(MessageInput in) throws ValidationException, IOException {
        // passes static helper functions into constructor
        this(LatitudeProtocol.readId(in), in.read(), in.read(), LatitudeProtocol.readLatString(in), LatitudeProtocol.readLatString(in));
    }
    
    /**
     * Constructs location record using user input
     * 
     * @param in user input source
     * @param out user output (prompt) destination
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public LocationRecord(Scanner in, PrintStream out) throws ValidationException {
        // same as default constructor, but supplied by a user interface
        out.print("User ID> ");
        this.setUserId( in.nextLong() );
        out.print("Longitude> ");
        this.setLongitude( in.next() );
        out.print("Latitude> ");
        this.setLatitude( in.next() );
        in.nextLine(); // must consume trailing newline
        out.print("Location Name> ");
        this.setLocationName( in.nextLine() );
        out.print("Location Description> ");
        this.setLocationDescription( in.nextLine() );
    }
    
    /**
     * Serializes location record
     * 
     * @param out serialization output destination
     * 
     * @throws IOException
     *      if I/O problem
     */
    public void encode(MessageOutput out) throws IOException {
        if(out == null) {
            throw new NullPointerException("null MessageOutput");
        }
        
        out.write(Long.toString(userId) + " ")
           .write(longitude + " ")
           .write(latitude + " ");
        LatitudeProtocol.writeLatString(out, locationName);
        LatitudeProtocol.writeLatString(out, locationDescription);
    }
    
    /**
     * Returns user ID
     * 
     * @returns user ID
     */
    public final long getUserId() {
        return userId;
    }
    
    /**
     * Sets user ID
     * 
     * @param userId new user ID
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public final void setUserId(long userId) throws ValidationException {
    	try {
        	this.userId = Filters.filterId(userId, Filters.UNSIGNED_INT_MAX_POWER);
        } catch(IllegalArgumentException e) {
        	throw new ValidationException(String.valueOf(userId), "Invalid mapID", e);
        }
    }

    /**
     * Returns longitude
     * 
     * @returns longitude
     */
    public final String getLongitude() {
        return longitude;
    }

    /**
     * Sets longitude
     * 
     * @param longitude new longitude
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public final void setLongitude(String longitude) throws ValidationException {
    	try {
        	this.longitude = String.valueOf( Filters.filterLongLat(longitude, Filters.LONGITUDE_MAX) );
        } catch(IllegalArgumentException e) {
        	throw new ValidationException(longitude, "Invalid longitude", e);
        }
    }

    /**
     * Returns latitude
     * 
     * @returns latitude
     */
    public final String getLatitude() {
        return latitude;
    }

    /**
     * Sets latitude
     * 
     * @param latitude new latitude
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public  final void setLatitude(String latitude) throws ValidationException {
    	try {
        	this.latitude = String.valueOf( Filters.filterLongLat(latitude, Filters.LATITUDE_MAX) );
        } catch(IllegalArgumentException e) {
        	throw new ValidationException(locationName, "Invalid latitude", e);
        }
    }

    /**
     * Returns location name
     * 
     * @returns location name
     */
    public final String getLocationName() {
        return locationName;
    }

    /**
     * Sets location name
     * 
     * @param locationName new location name
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public final void setLocationName(String locationName) throws ValidationException {
        try {
        	this.locationName = Filters.filterASCII(locationName);
        } catch(IllegalArgumentException e) {
        	throw new ValidationException(locationName, "Invalid location name", e);
        }
    }

    /**
     * Returns location description
     * 
     * @returns location description
     */
    public final String getLocationDescription() {
        return locationDescription;
    }

    /**
     * Sets location description
     * 
     * @param locationDescription new location description
     * 
     * @throws ValidationException
     *      if validation fails
     */
    public final void setLocationDescription(String locationDescription) throws ValidationException {
    	try {
        	this.locationDescription = Filters.filterASCII(locationDescription);
        } catch(IllegalArgumentException e) {
        	throw new ValidationException(locationName, "Invalid location description", e);
        }
    }
    
    @Override
    public String toString() {
        return "User " +  userId + ":" + locationName + " - " + locationDescription +
                " at (" + longitude + ", " + latitude + ")";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
        result = prime * result + ((locationDescription == null) ? 0 : locationDescription.hashCode());
        result = prime * result + ((locationName == null) ? 0 : locationName.hashCode());
        result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
        result = prime * result + (int) (userId ^ (userId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof LocationRecord))
            return false;
        LocationRecord other = (LocationRecord) obj;
        if (latitude == null) {
            if (other.latitude != null)
                return false;
        } else if (!latitude.equals(other.latitude))
            return false;
        if (locationDescription == null) {
            if (other.locationDescription != null)
                return false;
        } else if (!locationDescription.equals(other.locationDescription))
            return false;
        if (locationName == null) {
            if (other.locationName != null)
                return false;
        } else if (!locationName.equals(other.locationName))
            return false;
        if (longitude == null) {
            if (other.longitude != null)
                return false;
        } else if (!longitude.equals(other.longitude))
            return false;
        if (userId != other.userId)
            return false;
        return true;
    }
}
