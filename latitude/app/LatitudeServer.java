/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 3
* Class: CS 4321
*
************************************************/

package latitude.app;

import java.net.*;  // for Socket, ServerSocket, and InetAddress
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.io.*;   // for IOException and Input/OutputStream
import latitude.serialization.*;
import mapservice.MapManager;

/**
 * Server that sends and receives LatitudeMessages, and updates a map
 */
public class LatitudeServer extends LatitudeSuperServer implements Runnable {
    private static final int TIMEOUT = 5000;   // Resend timeout (milliseconds)

    // Socket connect to client
	private Socket clntSock;
	
	/**
	 * Constructs TCP LatitudeServer
	 * 
	 * @param clntSock client's port to connect to
	 */
    public LatitudeServer(MapManager mgr, Socket clntSock) {
    	// save reference
    	super(mgr);
		this.clntSock = clntSock;
	}

	/**
     * Defines Multithreaded Latitude Server
     * 
     * @param args port, thread count, and password filename
     * 
     * @throws SocketException
     *      if NoTiFiServer failed to start
     */
    public static void main(String[] args) throws SocketException {
        if (args.length != 3) {  // Test for correct # of args
            System.err.print("Parameter(s): <Port> <Thread Count> <Password Filename>");
            System.exit(0); // terminate
        }
        
        // init args
        int servPort = Integer.parseInt(args[0]),
    		nThreads = Integer.parseInt(args[1]);
        
        // server setup
        var mgr = setup(servPort, new File(args[2]));
        
        // init thread pool
        Executor service = Executors.newFixedThreadPool(nThreads);
      
        // Create a server socket to accept client connection requests
        try(var servSock = new ServerSocket(servPort)){
            
            // enable SO_REUSEADDR
            servSock.setReuseAddress(true);
            
            while (true) { // Run forever, accepting and servicing connections
        		// Get client connection
            	Socket clntSock = servSock.accept();
                clntSock.setSoTimeout(TIMEOUT); // Maximum receive blocking time (milliseconds)
                
            	// spawn new thread
            	service.execute(new LatitudeServer(mgr, clntSock));
              }
        } catch(Exception e) {
            System.err.print( e.getMessage() );
        }
        System.exit(0); // kills all threads
    }
    
    /**
     * Run LatitudeClient interaction
     */
    @Override
	public void run() {
		System.out.println("Handling client " + clntSock.getRemoteSocketAddress()
        	+ " with thread id " + String.valueOf( Thread.currentThread().getId() ));
    
		try {
		    while(true) { // Run forever, until EOS
		    	// pass client's InputStream into interact algorithm
		    	// returns proper LatitudeMessage to encode and send to client
		    	interact(mgr, clntSock.getInputStream())
		    		.encode(new MessageOutput( clntSock.getOutputStream() ));
		    }
		} catch(EOFException e) {
			// EOF means client finished
		} catch(Exception ex) {
        	try {
				new LatitudeError(0L, "Unable to parse message")
					.encode(new MessageOutput( clntSock.getOutputStream() ));
			} catch (Exception e) { // if failed to encode ERROR
				System.err.print( e.getMessage() );
			}
        }
	}
}
