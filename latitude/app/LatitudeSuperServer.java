/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 3
* Class: CS 4321
*
************************************************/

package latitude.app;

import java.io.*;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import latitude.serialization.*;
import mapservice.*;
import mapservice.Location.Color;
import notifi.app.NoTiFiServer;
import tools.BinaryTools;

/**
 * LatitudeSuperServer will handle all server overhead and client protocol interaction
 */
public abstract class LatitudeSuperServer {
	/**
     * File containing JS-encoded array of locations
     */
    private static final String LOCATIONFILE = "markers.js",
							  	MAPNAME = "Class Map", // default map name
						  		LOGFILE = "connections.log";
    // default color
    private static final Color COLOR  = Location.Color.BLUE;
    
    // specified mapID
    private static final long MAPID = 345;
    
    /**
     * Buffer size (bytes)
     */
    protected static final int BUFSIZE = BinaryTools.BUFSIZE;
    
	// data structures that will map userIDs to usernames and vice versa
    protected static Map<Long, String> userId2Name;
    protected static Map<String, Long> userName2Id;
    
    protected MapManager mgr;
    protected static int servPort;
    
    /**
     * Global logger
     */
    protected static final Logger logger = Logger.getLogger("Basic");
    
    /**
     * Saves reference to MapManager
     * 
     * @param mgr reference to MapManager to be updated
     */
    public LatitudeSuperServer(MapManager mgr) {
    	this.mgr = mgr;
    }
    
    /**
     * Sets up server database loading, MemoryMapManager, format logger, and NoTiFiServer
     * 
     * @param arg server port from command line
     * @param passwds password File
     * 
     * @return MapManager reference
     * 
     * @throws SocketException
     * 		if NoTiFiServer failed to start
     */
    protected static MapManager setup(int arg, File passwds) throws SocketException {
    	// init args
        servPort = arg;
        
        // load userData for instant access
        load(passwds);
        
        // Create map manager in memory
        var mgr = new MemoryMapManager();
        // Delete existing locations
        mgr.getLocations()
        	.parallelStream()
        	.forEach(l -> mgr.deleteLocation( l.getName() ));
        // Register listener to update MapBox location file
        mgr.register(new MapBoxObserver(LOCATIONFILE, mgr));
        
        // link LOGGER to logfile
        FileHandler fh;
        try {
            fh = new FileHandler(LOGFILE);
            logger.addHandler(fh);
            // human readable log
            fh.setFormatter(new SimpleFormatter());
        } catch (IOException e) {
            // failed to open logfile
            System.err.println( e.getMessage() );
        }
        
        // run NoTiFiServer
        new Thread(new NoTiFiServer(mgr, servPort)).start();
        
        return mgr;
    }
    
    /**
     * Defines interaction between LatitudeServers and clients
     * 
     * @param mgr MapManager to update
     * @param in InputStream client has sent bytes over
     * 
     * @return LatitudeMessage to send to client
     * 
     * @throws Exception
     * 		if parsing error
     */
    protected static LatitudeMessage interact(MapManager mgr, InputStream in) throws Exception {
    	// LatitudeMessage received from client
        LatitudeMessage received = LatitudeMessage.decode(new MessageInput(in)),
                        toSend = errFilters(received); // error-checked LatitudeMessage toSend to client
        
        // if ERROR, then return it -> less nesting
        if(toSend != null) {
        	return toSend;
        }
        
        if( LatitudeNewLocation.OPERATION.equals( received.getOperation() ) ) {
            // extract NEW LocationRecord
            var rcvdLocRec = ((LatitudeNewLocation)received).getLocationRecord();
            
            // load username from password file
            String name = userId2Name.get( rcvdLocRec.getUserId() ) + ": " + rcvdLocRec.getLocationName();
            
            // update map
            mgr.deleteLocation(name);
            // client's new Location
            var loc = new Location(name, rcvdLocRec.getLongitude(), rcvdLocRec.getLatitude(),
                    rcvdLocRec.getLocationDescription(), COLOR);
            // log update
            logger.info("New Location: " + loc);
            mgr.addLocation(loc);
        }
        // send a RESPONSE no matter what
        toSend = new LatitudeLocationResponse(received.getMapId(), MAPNAME);
        
        // add map locations to RESPONSE
        for(Location loc : mgr.getLocations()) {
            ((LatitudeLocationResponse)toSend).addLocationRecord( convertLoc(loc) );
        }
        
        return toSend;
    }
    
    /**
     * Filters rcvmsg in specified order of consideration
     * 
     * @param received LatitudeMessage to filter
     * 
     * @returns error message (if any)
     * 
     * @throws ValidationException
     *      if validation failure
     */
    protected static LatitudeMessage errFilters(LatitudeMessage received) throws ValidationException {
    	// client's LatitudeMessage operation
        String op = received.getOperation();
        switch(op) {
            case LatitudeError.OPERATION:
            case LatitudeLocationResponse.OPERATION:
                return new LatitudeError(received.getMapId(), "Unexpected message type: " + op);
        }
        
        if(received.getVersion() != LatitudeMessage.VERSION) {
            return new LatitudeError(0L, "Unexpected version: " + received.getVersion());
        }

        switch(op) {
            case LatitudeNewLocation.OPERATION:
            case LatitudeLocationRequest.OPERATION: break; // !(NEW || ALL) 
            default: return new LatitudeError(received.getMapId(), "Unknown operation: " + op);
        }
        
        if(received.getMapId() != MAPID) {
            return new LatitudeError(received.getMapId(), "No such map: " + received.getMapId());
        }
        
        if( LatitudeNewLocation.OPERATION.equals(op) ) {
        	// client's userID
            long userId = ((LatitudeNewLocation)received).getLocationRecord().getUserId();
            
            // if user is unregistered
            if(userId2Name.get(userId) == null) {
                return new LatitudeError(received.getMapId(), "No such user ID: " + String.valueOf(userId));
            }
        }
        
        return null; // no errors
    }
    
	/**
     * Load userData from password file into static structures
     * 
     * @param passFile local file of passwords
     */
    protected static void load(File passFile) {
        // init maps
        userId2Name = new HashMap<Long, String>();
        userName2Id = new HashMap<String, Long>();
        
        try(var passIn = new Scanner(passFile)) {
            while( passIn.hasNextLine() ) {
            	// user's info loaded into String[]
                String userdata[] = passIn.nextLine().split(":"),
                       userName = userdata[1];
                long userId = Long.parseLong(userdata[0]);
                
                userId2Name.put(userId, userName);
                userName2Id.put(userName, userId);
            }
        } catch (FileNotFoundException e) {
            System.err.print("File not found");
            System.exit(0); // terminate
        }
    }
    
    /**
     * Return userID extracted from Location
     * 
     * @param location map Location with corresponding username
     * 
     * @return user ID
     */
    protected static long getUserId(Location location) {
    	// split then immediately dereference for key
    	return userName2Id.get( location.getName().split(":")[0] );
    }
    
    /**
     * Converts a Location to a LocationRecord
     * 
     * @param location Location to convert
     * 
     * @returns converted LocationRecord
     * 
     * @throws ValidationException
     *      if validation failure
     */
    protected static LocationRecord convertLoc(Location location) throws ValidationException {
    	return new LocationRecord(getUserId(location), location.getLongitude(), location.getLatitude(),
                location.getName(), location.getDescription());
    }
}
