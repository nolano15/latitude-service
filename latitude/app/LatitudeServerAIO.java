/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 7
* Class: CS 4321
*
************************************************/

package latitude.app;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.logging.Level;
import latitude.serialization.*;
import mapservice.MapManager;

/**
 * TCP Latitude Server using Asynchronous I/O
 * 
 * The main() creates a TCP server socket channel, sets up the socket including
 * binding and setting the accept completion handler, and non-busily waits (forever).
 */
public class LatitudeServerAIO extends LatitudeSuperServer {
	private static final String DELIMITER = "\r\n";
	private Map<AsynchronousSocketChannel, MessageNIODeframer> clients;
	
	/**
	 * Constructs Asynchronous Latitude Server
	 * 
	 * @param mgr MapManager reference
	 */
	public LatitudeServerAIO(MapManager mgr) {
		// save reference
		super(mgr);
		// init data structure
		clients = new HashMap<AsynchronousSocketChannel, MessageNIODeframer>();
	}

    /**
     * Defines Asynchronous Latitude Server
     * 
     * @param args port and password filename
     * 
     * @throws IOException
     * 		if server setup or socket open/bind error
     */
    public static void main(String[] args) throws IOException {
        if (args.length != 2) { // Test for correct # of args
            throw new IllegalArgumentException("Parameter(s): <Port> <Password Filename>");
        }
        
        // init args
        int servPort = Integer.parseInt(args[0]);

        // Setup server overhead, pass its MapManager reference into constructor
        // then run Asynchronous Latitude Server
        new LatitudeServerAIO(setup(servPort, new File(args[1]))).run();
    }
    
    /**
     * Runs Asynchronous Latitude Server
     * 
     * @throws IOException
     * 		if server setup or socket open/bind error
     */
    private void run() throws IOException {
    	try (AsynchronousServerSocketChannel listenChannel = AsynchronousServerSocketChannel.open()) {
            // Bind local port
            listenChannel.bind(new InetSocketAddress(servPort));

            // Create accept handler
            listenChannel.accept(null, new CompletionHandler<AsynchronousSocketChannel, Void>() {

                @Override
                public void completed(AsynchronousSocketChannel clntChan, Void attachment) {
                    listenChannel.accept(null, this); // loop
                    
                    // add client with protocol-specified delimiter
                    clients.put(clntChan, getDeframer());
                    
                    try {
                        handleAccept(clntChan);
                    } catch (IOException e) {
                        failed(e, null);
                    }
                }

                @Override
                public void failed(Throwable e, Void attachment) {
                    logger.log(Level.WARNING, "Close Failed", e);
                }
            });
            // Block until current thread dies
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            logger.log(Level.WARNING, "Server Interrupted", e);
        }
    }

    /**
     * Called after each accept completion
     * 
     * @param clntChan channel of new client
     * @throws IOException if I/O problem
     */
    public void handleAccept(final AsynchronousSocketChannel clntChan) throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(BUFSIZE);
        clntChan.read(buf, buf, new CompletionHandler<Integer, ByteBuffer>() {
            public void completed(Integer bytesRead, ByteBuffer buf) {
                try {
                    handleRead(clntChan, buf, bytesRead);
                } catch (IOException e) {
                    logger.log(Level.WARNING, "Handle Read Failed", e);
                }
            }

            public void failed(Throwable ex, ByteBuffer v) {
                try {
                    clntChan.close();
                } catch (IOException e) {
                    logger.log(Level.WARNING, "Close Failed", e);
                }
            }
        });
    }

    /**
     * Called after each read completion
     * 
     * @param clntChan channel of new client
     * @param buf byte buffer used in read
     * @throws IOException if I/O problem
     */
    public void handleRead(final AsynchronousSocketChannel clntChan, ByteBuffer buf, int bytesRead)
            throws IOException {
        if (bytesRead == -1) { // Did the other end close?
        	clients.remove(clntChan); // remove client
            clntChan.close();
        } else if (bytesRead > 0) {
        	
        	// send bytes to deframer
        	byte[] deframed = clients.get(clntChan).nextMsg( buf.array() );
        	
        	// if a complete LatitudeMessage has not been received yet
        	if(deframed == null) {
        		handleAccept(clntChan); // read more
        	} else {
        		// give client a fresh Deframer
        		clients.put(clntChan, getDeframer());
        		
        		LatitudeMessage toSend = null;   		
        		try {
        			// init ByteInputStream with deframed bytes
        			// pass into interact algorithm
        			// returns proper LatitudeMessage to encode and send to client
        			toSend = interact(mgr, new ByteArrayInputStream(deframed));
				} catch (Exception ex) { // send ERROR
					try {
						toSend = new LatitudeError(0L, "Unable to parse message");
					} catch (Exception e) { // if failed to encode ERROR
						System.err.print( e.getMessage() );
					}
				}
        		
        		var bOut = new ByteArrayOutputStream();
                
                // encode LatitudeMessage
                toSend.encode(new MessageOutput(bOut));
                
                // wrap into a ByteBuffer before writing
                ByteBuffer v = ByteBuffer.wrap( bOut.toByteArray() );
                
                clntChan.write(v, v, new CompletionHandler<Integer, ByteBuffer>() {
                    public void completed(Integer bytesWritten, ByteBuffer v) {
                        try {
                            handleWrite(clntChan, v);
                        } catch (IOException e) {
                            logger.log(Level.WARNING, "Handle Write Failed", e);
                        }
                    }

                    public void failed(Throwable ex, ByteBuffer v) {
                        try {
                            clntChan.close();
                        } catch (IOException e) {
                            logger.log(Level.WARNING, "Close Failed", e);
                        }
                    }
                });
        	}
        }
    }

    /**
     * Called after each write
     * 
     * @param clntChan channel of new client
     * @param buf byte buffer used in write
     * @throws IOException if I/O problem
     */
    public void handleWrite(final AsynchronousSocketChannel clntChan, ByteBuffer buf) throws IOException {
        if ( buf.hasRemaining() ) { // More to write
            clntChan.write(buf, buf, new CompletionHandler<Integer, ByteBuffer>() {
                public void completed(Integer bytesWritten, ByteBuffer buf) {
                    try {
                        handleWrite(clntChan, buf);
                    } catch (IOException e) {
                        logger.log(Level.WARNING, "Handle Write Failed", e);
                    }
                }

                public void failed(Throwable ex, ByteBuffer buf) {
                    try {
                        clntChan.close();
                    } catch (IOException e) {
                        logger.log(Level.WARNING, "Close Failed", e);
                    }
                }
            });
        } else { // Back to reading
        	handleAccept(clntChan);
        }
    }
    
    /**
     * Factory method that returns fresh Deframer
     * 
     * @return deframer
     */
    private MessageNIODeframer getDeframer() {
    	try {
			return new MessageNIODeframer( DELIMITER.getBytes(MessageInput.CHARENC) );
		} catch (UnsupportedEncodingException e) {
			// ASCII will be supported
		}
    	return null; // compiler
    }
}